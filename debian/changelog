perlbrew (1.01-1) unstable; urgency=medium

  * Import upstream version 1.01.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Nov 2024 02:26:09 +0100

perlbrew (1.00-1) unstable; urgency=medium

  * Import upstream version 1.00.
  * Bump versioned (test) dependency on libcpan-perl-releases-perl.

 -- gregor herrmann <gregoa@debian.org>  Wed, 09 Oct 2024 18:13:44 +0200

perlbrew (0.99-1) unstable; urgency=medium

  * Import upstream version 0.99.
  * Update years of upstream and packaging copyright.
  * Update test and runtime dependencies.
  * Refresh autopkgtest-path.patch (offset).

 -- gregor herrmann <gregoa@debian.org>  Wed, 25 Sep 2024 21:42:50 +0200

perlbrew (0.98-1) unstable; urgency=medium

  * Import upstream version 0.98.
  * Bump versioned (test) dependency on libcpan-perl-releases-perl.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Aug 2023 21:30:29 +0200

perlbrew (0.97-1) unstable; urgency=medium

  * Import upstream version 0.97.
  * Update (build) dependencies.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Mon, 12 Jun 2023 16:44:33 +0200

perlbrew (0.96-1) unstable; urgency=medium

  * Import upstream version 0.96.
  * Bump versioned (test) dependency on libcpan-perl-releases-perl.

 -- gregor herrmann <gregoa@debian.org>  Wed, 17 Aug 2022 20:31:24 +0200

perlbrew (0.95-1) unstable; urgency=medium

  * Import upstream version 0.95.
  * Update years of upstream and packaging copyright.
  * Update versioned (build) dependency in libcpan-perl-releases-perl.
  * Declare compliance with Debian Policy 4.6.1.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 May 2022 00:20:46 +0200

perlbrew (0.94-1) unstable; urgency=medium

  * Import upstream version 0.94.
  * Remove workaround for stray ./Build file which is removed in this
    release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 05 Dec 2021 01:09:49 +0100

perlbrew (0.93-1) unstable; urgency=medium

  * Import upstream version 0.93.
  * Bump versioned (build) dependency on libcpan-perl-releases-perl.
  * debian/rules: handle stray ./Build file in upstream tarball.

 -- gregor herrmann <gregoa@debian.org>  Thu, 25 Nov 2021 18:00:28 +0100

perlbrew (0.92-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libcapture-tiny-perl and
      libfile-which-perl.
    + perlbrew: Drop versioned constraint on libcapture-tiny-perl in Depends.

  [ gregor herrmann ]
  * Import upstream version 0.92.
  * Update test and runtime dependencies.
  * Declare compliance with Debian Policy 4.6.0.

 -- gregor herrmann <gregoa@debian.org>  Thu, 30 Sep 2021 12:56:22 +0200

perlbrew (0.91-1) unstable; urgency=medium

  * Import upstream version 0.91.
  * Update years of upstream and packaging copyright.
  * Bump versioned (build) dependencies.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Feb 2021 17:08:23 +0100

perlbrew (0.89-2) unstable; urgency=medium

  * Team upload

  * add 'patch' to run-time dependencies.
    Thanks to Aaron Hall for the report (Closes: #975364)

 -- Damyan Ivanov <dmn@debian.org>  Sat, 21 Nov 2020 15:01:18 +0000

perlbrew (0.89-1) unstable; urgency=medium

  * Import upstream version 0.89.
  * Update versioned test and runtime dependencies.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sat, 31 Oct 2020 16:11:41 +0100

perlbrew (0.88-1) unstable; urgency=medium

  * Import upstream version 0.88.
  * Update years of upstream and packaging copyright.
  * Update versioned (build) dependencies.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- gregor herrmann <gregoa@debian.org>  Sat, 08 Feb 2020 13:36:08 +0100

perlbrew (0.87-1) unstable; urgency=medium

  * Import upstream version 0.87.
  * debian/*: replace ADTTMP with AUTOPKGTEST_TMP.
  * Update {versioned,build} dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Drop unneeded version constraints from (build) dependencies.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Nov 2019 04:31:11 +0100

perlbrew (0.86-1) unstable; urgency=medium

  * Import upstream version 0.86.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.

 -- gregor herrmann <gregoa@debian.org>  Mon, 21 Jan 2019 19:05:15 +0100

perlbrew (0.85-1) unstable; urgency=medium

  * Import upstream version 0.85
  * Update debian/upstream/metadata.
  * Refresh autopkgtest-path.patch.
  * Install new CONTRIBUTING document.
  * Declare compliance with Debian Policy 4.2.1.
  * Don't install empty manpages.

 -- gregor herrmann <gregoa@debian.org>  Sat, 15 Dec 2018 22:18:49 +0100

perlbrew (0.84-1) unstable; urgency=medium

  * Import upstream version 0.84.
  * Drop 0001-Fix-a-test-failures-caused-by-not-preserving-file-mo.patch
    which was taken from upstream Git.
  * Update autopkgtest-path.patch: changed path.
  * debian/rules: drop workaround for installing perlbrew(1).
    The issue is fixed in this release.
  * Update versioned (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 Jun 2018 19:33:16 +0200

perlbrew (0.83-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.83.
  * Update years of upstream and packaging copyright.
  * Add new build dependencies.
  * debian/rules: remove override_dh_auto_build.
    Recreating the manpage doesn't seem to be necessary anymore.
  * Add patch from upstream Git repo to fix permissions of test file.
  * debian/rules: work around installation problem. (GH#619)
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Jun 2018 17:44:15 +0200

perlbrew (0.82-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.2 (no changes needed)

  [ gregor herrmann ]
  * Import upstream version 0.82.

 -- gregor herrmann <gregoa@debian.org>  Tue, 26 Dec 2017 19:58:06 +0100

perlbrew (0.80-1) unstable; urgency=medium

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ gregor herrmann ]
  * Import upstream version 0.80.
  * Add /me to Uploaders.
  * Update (build) dependencies.
  * Build depend on both curl and wget.
    There's a new test looking for wget (or fetch) in the absence of curl.
  * Declare compliance with Debian Policy 4.1.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 01 Nov 2017 17:27:52 +0100

perlbrew (0.78-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.78.

 -- gregor herrmann <gregoa@debian.org>  Mon, 21 Nov 2016 19:22:46 +0100

perlbrew (0.77-2) unstable; urgency=medium

  * Team upload.
  * Add --name parameter to pod2man call in debian/rules in order to make
    the build reproducible.
    Thanks to Chris Lamb for the bug report and the patch. (Closes: #844992)

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Nov 2016 20:47:41 +0100

perlbrew (0.77-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.77.
    Fixes "Unescaped left brace in regex is deprecated"
    (Closes: #826500)
  * Drop 5.003_07.patch, fixed upstream.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Tue, 15 Nov 2016 20:19:00 +0100

perlbrew (0.76-2) unstable; urgency=medium

  * Team upload.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Add patch to handle removal of perl 5.003_07 from the CPAN.
    (Closes: #832832)

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Jul 2016 18:39:23 +0200

perlbrew (0.76-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Lucas Kanashiro ]
  * Import upstream version 0.76
  * Remove patch fixing typo in manpage, applied by upstream
  * Declare compliance with Debian policy 3.9.8
  * debian/control: update build and runtime dependencies

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 27 Jun 2016 17:43:53 -0300

perlbrew (0.75-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Import upstream version 0.75
  * d/u/metadata: fix email syntax in Contact field
  * Update years of upstream copyright
  * Update Debian packaging copyright
  * Declare compliance with Debian policy 3.9.7
  * Create patch to fix spelling error in manpage

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 21 Mar 2016 12:47:48 -0300

perlbrew (0.74-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.74
  * Bump debhelper compatibility level to 9

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 24 Nov 2015 23:45:18 -0200

perlbrew (0.73-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.73.
  * Drop spelling.patch, merged upstream.
  * Update years of upstream copyright.
  * Update versioned build and runtime dependencies.
  * Mark package as autopkgtest-able.
  * Add patch to use installed script in tests for autopktest.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 May 2015 17:30:57 +0200

perlbrew (0.71-1) unstable; urgency=medium

  [ gregor herrmann ]
  * New upstream releases 0.68, 0.69.
  * Drop patches, both merged upstream.
  * Update years of upstream copyright.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata

  * Imported upstream version 0.71
  * Update (build) dependencies.
  * Add a patch to fix a spelling mistake.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Wed, 01 Oct 2014 20:19:44 +0200

perlbrew (0.67-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 0.67 (Closes: #746138)
  * Update versioned (Build-)Depends(-Indep) for liblocal-lib-perl.
    Update the versioned Build-Depends-Indep and Depends on
    liblocal-lib-perl to (>= 1.008026).
  * Bump Standards-Version to 3.9.5

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Alessandro Ghedini ]
  * Add 01_fix-spelling.patch to fix spelling-error-in-manpage
  * Add 02_fix-pod.patch to fix manpage-has-errors-from-pod2man

 -- Alessandro Ghedini <ghedo@debian.org>  Sun, 27 Apr 2014 17:07:29 +0200

perlbrew (0.66-1) unstable; urgency=low

  * New upstream release
  * (Build-)Depends on curl | wget

 -- Alessandro Ghedini <ghedo@debian.org>  Mon, 26 Aug 2013 18:11:37 +0200

perlbrew (0.65-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Update (build) dependencies.
  * Update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 03 Aug 2013 00:31:22 +0200

perlbrew (0.64-1) unstable; urgency=low

  * Team upload.

  * New upstream release.
    Fixes "FTBFS: tests failed" (Closes: #713265)
  * Update versioned (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Jun 2013 20:59:03 +0200

perlbrew (0.59-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Add build dependency on libtest-nowarnings-perl.

 -- gregor herrmann <gregoa@debian.org>  Mon, 18 Feb 2013 20:09:37 +0100

perlbrew (0.58-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 0.58

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 25 Nov 2012 11:07:32 +0100

perlbrew (0.57-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 0.57

 -- Salvatore Bonaccorso <carnil@debian.org>  Wed, 14 Nov 2012 22:10:59 +0100

perlbrew (0.55-1) unstable; urgency=low

  * New upstream release
  * libfile-path-tiny-perl is no longer required
  * Bump versioned (build) depends libcpan-perl-releases-perl
  * Drop fix-spelling.patch (no more needed)

 -- Alessandro Ghedini <ghedo@debian.org>  Wed, 07 Nov 2012 19:42:46 +0100

perlbrew (0.51-1) unstable; urgency=low

  * New upstream release
  * Bump versioned (build) depends on libcpan-perl-releases-perl and
    libcapture-tiny-perl
  * Bump Standards-Version to 3.9.4 (no changes needed)

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 09 Oct 2012 11:33:53 +0200

perlbrew (0.50-1) unstable; urgency=low

  * New upstream release
  * Update versioned (Build-)Depends
  * Add fix-spelling.patch

 -- Alessandro Ghedini <ghedo@debian.org>  Mon, 03 Sep 2012 11:41:10 +0200

perlbrew (0.46-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Alessandro Ghedini ]
  * New upstream release

 -- Alessandro Ghedini <ghedo@debian.org>  Thu, 26 Jul 2012 01:26:24 +0200

perlbrew (0.43-1) unstable; urgency=low

  * New upstream release
  * Email change: Alessandro Ghedini -> ghedo@debian.org
  * Update versioned (build) depends on libcapture-tiny-perl

 -- Alessandro Ghedini <ghedo@debian.org>  Wed, 30 May 2012 00:00:08 +0200

perlbrew (0.42-1) unstable; urgency=low

  * New upstream release
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3
  * Update copyright years for inc/Module/*

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 09 Mar 2012 15:13:32 +0100

perlbrew (0.41-1) unstable; urgency=low

  * Team upload.
  * Imported Upstream version 0.41
  * Update copyright years for upstream files

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 02 Feb 2012 21:59:26 +0100

perlbrew (0.40-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Add (build) dependency on libcpan-perl-releases-perl.

 -- gregor herrmann <gregoa@debian.org>  Mon, 23 Jan 2012 22:38:33 +0100

perlbrew (0.39-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release 0.38

  [ Alessandro Ghedini ]
  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 01 Jan 2012 17:36:19 +0100

perlbrew (0.37-1) unstable; urgency=low

  * New upstream release
  * Remove fix-typo.patch (merged upstream)
  * Add libcapture-tiny-perl to (Build-)Depends(-Indep)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 28 Dec 2011 17:06:03 +0100

perlbrew (0.35-1) unstable; urgency=low

  [ gregor herrmann ]
  * Swap order of alternative (build) dependencies after the perl 5.14
    transition.

  [ Alessandro Ghedini ]
  * New upstream release
  * (Build-)Depends on liblocal-lib-perl >= 1.008000
  * Add fix-typo patch

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sat, 17 Dec 2011 12:34:31 +0100

perlbrew (0.31-1) unstable; urgency=low

  * New upstream release
  * Remove libtext-levenshtein-perl from (B-)D(-I)
  * Add perl-doc to B-D-I and Recommends

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 06 Nov 2011 13:24:13 +0100

perlbrew (0.30-2) unstable; urgency=low

  * Fix long description (Closes: #645390)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Mon, 17 Oct 2011 19:33:34 +0200

perlbrew (0.30-1) unstable; urgency=low

  * New upstream release
  * Add liblocal-lib-perl and libtext-levenshtein-perl to
    (Build-)Depends(-Indep)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 14 Oct 2011 12:15:12 +0200

perlbrew (0.29-1) unstable; urgency=low

  * New upstream release
  * Build depends on libtest-simple-perl (>= 0.98)
  * Add also libc-dev to Depends (needed to build perl)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 18 Sep 2011 17:30:13 +0200

perlbrew (0.28-2) unstable; urgency=low

  * Add make and gcc to Depends (Closes: #638655)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sat, 20 Aug 2011 18:59:36 +0200

perlbrew (0.28-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Alessandro Ghedini ]
  * New upstream release
  * (Build-)Depends on libfile-path-tiny-perl

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 10 Aug 2011 19:45:27 +0200

perlbrew (0.27-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Mon, 11 Jul 2011 20:10:21 +0200

perlbrew (0.24-1) unstable; urgency=low

  * New upstream release
  * Add libtest-spec-perl to Build-Depends-Indep

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 12 Jun 2011 13:13:04 +0200

perlbrew (0.23-1) unstable; urgency=low

  * New upstream release
  * Remove libtry-tiny-perl from B-D-I

 -- Alessandro Ghedini <al3xbio@gmail.com>  Tue, 31 May 2011 19:35:28 +0200

perlbrew (0.22-1) unstable; urgency=low

  * New upstream release
  * Add libio-all-perl, libtry-tiny-perl, libpath-class-perl and
    libtest-excpetion-perl to B-D-I

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 27 May 2011 18:53:49 +0200

perlbrew (0.21-1) unstable; urgency=low

  [ Alessandro Ghedini ]
  * New upstream release

  [ gregor herrmann ]
  * Update years of copyright for inc/Module/*.

 -- Alessandro Ghedini <al3xbio@gmail.com>  Tue, 17 May 2011 17:53:10 +0200

perlbrew (0.20-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Thu, 12 May 2011 16:41:06 +0200

perlbrew (0.19-1) unstable; urgency=low

  * New upstream release
  * Update dependencies:
    - (Build-)Depends(-Indep) on libdevel-patchperl-perl
    - B-D-I on libtest-output-perl
  * Update upstream copyright years
  * Bump Standards-Version to 3.9.2 (no changes needed)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 29 Apr 2011 11:02:55 +0200

perlbrew (0.18-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Wed, 16 Mar 2011 12:12:38 +0100

perlbrew (0.17-1) unstable; urgency=low

  * New upstream release
  * Bump debhelper to 8

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sat, 12 Mar 2011 13:16:58 +0100

perlbrew (0.16-1) unstable; urgency=low

  * Initial Release. (Closes: #609424)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 9 Jan 2011 13:27:33 +0100
